# Section B: React JS Task

## Task 1 - Creating the folder structure

### The Provided JSON file
```javascript
[
"Documents": [
        "Document1.jpg",
        "Document2.jpg",
        "Document3.jpg"
],
"Desktop": [
        "Screenshot1.jpg",
        "videopal.mp4"
],
"Downloads": [
        "Drivers": [
                "Printerdriver.dmg",
                "cameradriver.dmg"
],
"Applications": [
        "Webstorm.dmg",
        "Pycharm.dmg",
        "FileZila.dmg",
        "Mattermost.dmg",
],
"chromedriver.dmg"
]
]
```

As "src" is the default React.js folder I didn't delete, you can find all the folders and files inside the "src" folder, 
and all of them are structured as mentioned in the task and the above-mentioned JSON file.

## Please note that I have created some dummy files to be inside the folder, just as an example.